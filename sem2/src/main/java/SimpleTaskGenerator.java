//класс SimpleTaskGenerator, реализующий интерфейс генератора
public class SimpleTaskGenerator implements ITaskGenerator {

    private IBuffer buffer; //Ссылка на буфер
    private int startValue; //целое число1
    private int amount; //целое число2


    //Конструктор класса получает на вход ссылку на буфер и числа startValue и amount
    public SimpleTaskGenerator(IBuffer buffer, int startValue, int amount) {
        setBuffer(buffer);
        setStartValue(startValue);
        setAmount(amount);
    }

    private void setBuffer(IBuffer buffer) {
        if (buffer == null)
            throw new NullPointerException("Null buffer");
        this.buffer = buffer;
    }

    private void setStartValue(int startValue) {
        this.startValue = startValue;
    }

    private void setAmount(int amount) {
        if (amount < 0)
            throw new IllegalArgumentException("Amount can't be negative"); //Целое число Amount не может быть отрицательным
        this.amount = amount;
    }

    public SimpleTaskGenerator withStartValue(int startValue) {
        setStartValue(startValue);
        return this;
    }

    public SimpleTaskGenerator withAmount(int amount) {
        setAmount(amount);
        return this;
    }

    //Метод generate создает задачу, данные в которой — это массив чисел
    //вида startValue, startValue + 1, … startValue + amount – 1
    //Созданная задача помещается в буфер
    @Override
    public void generate() {
        int[] data = new int[amount];
        for (int i = 0; i < amount; i++){
            data[i] = startValue + i;
        }
        buffer.addElem(new Task(data));
    }
}
