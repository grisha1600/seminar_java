import java.util.LinkedList;
import java.util.Queue;

//класс SimpleBuffer, реализующий интерфейс буфера на базе очереди
public class SimpleBuffer implements IBuffer{

    private Queue<Task> tasks;

    public SimpleBuffer() {
        this.tasks = new LinkedList<>();
    }

    @Override
    public void addElem(Task elem) {
        tasks.add(elem);
    }

    @Override
    public Task getElem() {
        return tasks.remove();
    }

    @Override
    public int quantity(){
        return tasks.size();
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public boolean isEmpty() {
        return tasks.isEmpty();
    }
}
