//реализация процессора — класс SimpleTaskProcessor. Класс хранит ссылку на буфер .
// Метод забирает задачу из буфера, если она там
//есть, и вычисляет сумму элементов массива данных в задаче. Если буфер пуст, то метод
//process должен вернуть значение null.
public class SimpleTaskProcessor implements ITaskProcessor{
    private IBuffer buffer;


    public SimpleTaskProcessor(IBuffer buffer) {
        if (buffer == null)
            throw new NullPointerException("Null buffer");
        this.buffer = buffer;
    }

//Метод забирает задачу из буфера, если она там есть, и вычисляет сумму элементов массива данных в задаче.
// Если буфер пуст, то метод process должен вернуть значение null.
    @Override
    public Integer process() {
        if (buffer.quantity() == 0)
            return null;

        int sum = 0;
        for (int number: buffer.getElem().getData()) {
            sum += number;
        }

        return sum;
    }
}

