import java.util.Arrays;

//класс Task, реализующий интерфейс ITask. Класс хранит массив целых чисел
//Методы: конструктор по int, геттер для данных, equals и hashCode
public class Task implements ITask{

    private int[] k; //k-коэффициент


    public Task(int... k) {
        this.k = k;
    }

    @Override
    public int[] getData() {
        return k;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Arrays.equals(k, task.k);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(k);
    }
}
