import org.junit.Test;

import static org.junit.Assert.*;

public class TestSimpleBuffer {
    @Test
    public void testQuantityAndIsEmpty() {
        IBuffer buffer = new SimpleBuffer();
        SimpleBuffer buffer1 = new SimpleBuffer();

        assertEquals(0, buffer.quantity());
        assertEquals(0, buffer1.quantity());
        assertTrue(buffer.isEmpty());
        assertTrue(buffer1.isEmpty());
    }
    @Test
    public void testAddGetElem() {
        Task task1 = new Task();
        Task task2 = new Task(5,1,9);
        Task task3 = new Task(22,-11,0,1,3);

        IBuffer buffer = new SimpleBuffer();

        assertTrue(buffer.isEmpty());
        assertEquals(0, buffer.quantity());

        buffer.addElem(task1);
        buffer.addElem((Task) task2);

        assertFalse(buffer.isEmpty());
        assertEquals(2, buffer.quantity());

        buffer.addElem(task3);

        assertFalse(buffer.isEmpty());
        assertEquals(3, buffer.quantity());
        assertEquals(task1, buffer.getElem());
        assertEquals(task2, buffer.getElem());
        assertEquals(task3, buffer.getElem());
        assertEquals(0, buffer.quantity());
        assertTrue(buffer.isEmpty());
    }


    @Test
    public void testClear() {
        Task task1 = new Task();
        Task task2 = new Task(5,1,9);
        Task task3 = new Task(22,-11,0,1,3);

        SimpleBuffer buffer = new SimpleBuffer();

        buffer.addElem(task1);
        buffer.addElem((Task) task2);
        buffer.addElem(task3);

        buffer.clear();

        assertEquals(0, buffer.quantity());
        assertTrue(buffer.isEmpty());
    }
}
