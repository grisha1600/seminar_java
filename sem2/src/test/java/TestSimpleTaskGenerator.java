import org.junit.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class TestSimpleTaskGenerator {
    @Test
    public void testSimpleTaskGenerator1() {
        SimpleBuffer buffer = new SimpleBuffer();

        assertDoesNotThrow(() -> new SimpleTaskGenerator(buffer, 0, 1));

        try{
            SimpleTaskGenerator generator = new SimpleTaskGenerator(null,0,1);
        }
        catch (NullPointerException e1){
            assertEquals("Null buffer", e1.getMessage());
        }
        try{
            SimpleTaskGenerator generator1 = new SimpleTaskGenerator(buffer,0,-1);
        }
        catch (IllegalArgumentException e2){
            assertEquals("Amount can't be negative", e2.getMessage());
        }
    }

    @Test
    public void testSimpleTaskGenerator2() {
        SimpleBuffer buffer = new SimpleBuffer();
        SimpleTaskGenerator generator = new SimpleTaskGenerator(buffer, 1, 2);
        assertDoesNotThrow((Executable) () -> generator.withStartValue(2).withAmount(3));
        try{
            generator.withStartValue(2).withAmount(-2);
        }
        catch (IllegalArgumentException e){
            assertEquals("Amount can't be negative", e.getMessage());
        }
    }

    @Test
    public void testGenerate() {
        SimpleBuffer buffer = new SimpleBuffer();
        SimpleBuffer buffer1 = new SimpleBuffer();
        SimpleBuffer buffer2 = new SimpleBuffer();

        new SimpleTaskGenerator(buffer, 1, 0).generate();
        ITaskGenerator generator = new SimpleTaskGenerator(buffer1, 1, 2);
        new SimpleTaskGenerator(buffer2, -5, 11).generate();

        generator.generate();

        assertArrayEquals(new int[]{}, buffer.getElem().getData());
        assertArrayEquals(new int[]{1, 2}, buffer1.getElem().getData());
        assertArrayEquals(new int[]{-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5}, buffer2.getElem().getData());
    }

    @Test
    public void testGenerateWithStartValueAndAmount() {
        SimpleBuffer buffer = new SimpleBuffer();
        SimpleBuffer buffer1 = new SimpleBuffer();
        SimpleBuffer buffer2 = new SimpleBuffer();

        ITaskGenerator generator = new SimpleTaskGenerator(buffer, 1, 2);
        SimpleTaskGenerator generator1 = new SimpleTaskGenerator(buffer1, 5, 2);
        SimpleTaskGenerator generator2 = new SimpleTaskGenerator(buffer2, 1, 1);
        ((SimpleTaskGenerator) generator).withStartValue(2).generate();
        generator1.withAmount(4).generate();
        generator2.withStartValue(-2).withAmount(5).generate();

        assertArrayEquals(new int[]{2, 3}, buffer.getElem().getData());
        assertArrayEquals(new int[]{5, 6, 7, 8}, buffer1.getElem().getData());
        assertArrayEquals(new int[]{-2, -1, 0, 1, 2}, buffer2.getElem().getData());
    }

    @Test
    public void testGenerateWithFilledBuffer() {
        Task task = new Task(0, 0, 0);
        SimpleBuffer buffer = new SimpleBuffer();
        ITaskGenerator generator = new SimpleTaskGenerator(buffer, 0, 3);

        buffer.addElem(task);

        generator.generate();

        assertEquals(task, buffer.getElem());
        assertArrayEquals(new int[]{0, 1, 2}, buffer.getElem().getData());
    }
}
