import java.util.Objects;

//производный класс Derived с доп.строковым полем additionalDescription (доп.описание).
//В кл. конструкторы, геттеры, equals и hashCode.
public class Derived extends Base{
    private String additionalDescription;

    public Derived(String description, String additionalDescription) {
        super(description);
        this.additionalDescription = additionalDescription;
    }

    public String getAdditionalDescription() {
        return additionalDescription;
    }

    public void setAdditionalDescription(String additionalDescription) {
        this.additionalDescription = additionalDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Derived derived = (Derived) o;
        return Objects.equals(additionalDescription, derived.additionalDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), additionalDescription);
    }
}
