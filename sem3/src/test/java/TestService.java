import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestService {

    private static final List<TableString> table1 = new ArrayList<>();
    private static final List<TableString> table2 = new ArrayList<>();
    private static final List<TableString> table3 = new ArrayList<>();

    private static final Reference certificate1 = new Reference("Григорьева Ирина", "ОмГУ", "ИМИТ", "Прикладная математика и информатика",
            "01.09.2019", "22.06.2023", table1);
    private static final Reference certificate2 = new Reference("Короленко Ирина", "ОмГУ", "ИМИТ", "Прикладная математика и информатика",
            "01.09.2020", "22.06.2024", table2);
    private static final Reference certificate3 = new Reference("Лукина Ксения", "ОмГУ", "ИМИТ", "Прикладная математика и информатика",
            "01.09.2021", "22.06.2025", table3);

    @BeforeAll
    static void initTable1(){
        table1.add(new TableString("Математический анализ", 144, Mark.PASS_EXCELLENT));
        table1.add(new TableString("ЯМПЫ", 228, Mark.PASS_GOOD));
        table1.add(new TableString("ДММЛ", 144, Mark.PASS_EXCELLENT));
        table1.add(new TableString("С++", 228, Mark.SET_OFF_PASS));
        table1.add(new TableString("Алгебра и геометрия", 144, Mark.PASS_GOOD));
    }

    @BeforeAll
    static void initTable2(){
        table2.add(new TableString("Функциональный анализ", 144, Mark.PASS_EXCELLENT));
        table2.add(new TableString("ЯМПЫ", 228, Mark.PASS_GOOD));
        table2.add(new TableString("История", 144, Mark.PASS));
        table2.add(new TableString("С++", 228, Mark.SET_OFF_FAIL));
        table2.add(new TableString("Теория вероятности", 228, Mark.PASS));
    }

    @Test
    public void testGetDisciplines() {
        List<String> disciplines1 = new ArrayList<>(5);
        List<String> disciplines2 = new ArrayList<>(5);
        List<String> disciplines3 = new ArrayList<>(0);

        disciplines1.add("Математический анализ");
        disciplines1.add("ЯМПЫ");
        disciplines1.add("ДММЛ");
        disciplines1.add("С++");
        disciplines1.add("Алгебра и геометрия");

        disciplines2.add("Функциональный анализ");
        disciplines2.add("ЯМПЫ");
        disciplines2.add("История");
        disciplines2.add("С++");
        disciplines2.add("Теория вероятности");

        assertEquals(disciplines1, Service.getDisciplines(certificate1));
        assertEquals(disciplines2, Service.getDisciplines(certificate2));
        assertEquals(disciplines3, Service.getDisciplines(certificate3));
    }

    @Test
    public void testGetSumWorkload() {
        assertEquals(888, Service.getSumLaborInput(certificate1));
        assertEquals(972, Service.getSumLaborInput(certificate2));
        assertEquals(0, Service.getSumLaborInput(certificate3));
    }

    @Test
    public void testGetAverageMark() {
        assertEquals(4.5, Service.getMiddleMark(certificate1), 1e-9);
        assertEquals(3.75, Service.getMiddleMark(certificate2), 1e-9);
        assertEquals(0, Service.getMiddleMark(certificate3), 1e-9);
    }

    @Test
    public void testGetDisciplineMarkMap() {
        Map<String, String> disciplineMarkMap1 = new HashMap<>(5);
        Map<String, String> disciplineMarkMap2 = new HashMap<>(5);
        Map<String, String> disciplineMarkMap3 = new HashMap<>(0);

        disciplineMarkMap1.put("Математический анализ", "Отлично");
        disciplineMarkMap1.put("ЯМПЫ", "Хорошо");
        disciplineMarkMap1.put("ДММЛ", "Отлично");
        disciplineMarkMap1.put("С++", "Зачтено");
        disciplineMarkMap1.put("Алгебра и геометрия", "Хорошо");

        disciplineMarkMap2.put("Функциональный анализ", "Отлично");
        disciplineMarkMap2.put("ЯМПЫ", "Хорошо");
        disciplineMarkMap2.put("История", "Удовлетворительно");
        disciplineMarkMap2.put("С++", "Не зачтено");
        disciplineMarkMap2.put("Теория вероятности", "Удовлетворительно");

        assertEquals(disciplineMarkMap1, Service.getDisciplineMarkMap(certificate1));
        assertEquals(disciplineMarkMap2, Service.getDisciplineMarkMap(certificate2));
        assertEquals(disciplineMarkMap3, Service.getDisciplineMarkMap(certificate3));
    }

    @Test
    public void testGetMarkDisciplineMap() {
        Map<String, List<String>> markDisciplineMap1 = Service.getMarkDisciplineMap(certificate1);
        Map<String, List<String>> markDisciplineMap2 = Service.getMarkDisciplineMap(certificate2);
        Map<String, List<String>> markDisciplineMap3 = Service.getMarkDisciplineMap(certificate3);

        assertEquals(Arrays.asList("Математический анализ", "ДММЛ"), markDisciplineMap1.get("Отлично"));
        assertEquals(Arrays.asList("ЯМПЫ", "Алгебра и геометрия"), markDisciplineMap1.get("Хорошо"));
        assertTrue(markDisciplineMap1.get("Удовлетворительно").isEmpty());
        assertTrue(markDisciplineMap1.get("Неудовлетворительно").isEmpty());
        assertEquals(Collections.singletonList("С++"), markDisciplineMap1.get("Зачтено"));
        assertTrue(markDisciplineMap1.get("Не зачтено").isEmpty());

        assertEquals(Collections.singletonList("Функциональный анализ"), markDisciplineMap2.get("Отлично"));
        assertEquals(Collections.singletonList("ЯМПЫ"), markDisciplineMap2.get("Хорошо"));
        assertEquals(Arrays.asList("История", "Теория вероятности"), markDisciplineMap2.get("Удовлетворительно"));
        assertTrue(markDisciplineMap2.get("Неудовлетворительно").isEmpty());
        assertTrue(markDisciplineMap2.get("Зачтено").isEmpty());
        assertEquals(Collections.singletonList("С++"), markDisciplineMap2.get("Не зачтено"));

        assertTrue(markDisciplineMap3.get("Отлично").isEmpty());
        assertTrue(markDisciplineMap3.get("Хорошо").isEmpty());
        assertTrue(markDisciplineMap3.get("Удовлетворительно").isEmpty());
        assertTrue(markDisciplineMap3.get("Неудовлетворительно").isEmpty());
        assertTrue(markDisciplineMap3.get("Зачтено").isEmpty());
        assertTrue(markDisciplineMap3.get("Не зачтено").isEmpty());
    }
}
