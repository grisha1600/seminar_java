import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestReference {
        @Test
        public void testReference1() {
            List<TableString> table = new ArrayList<>(3);
            table.add(new TableString("Матанализ", 144, Mark.PASS_GOOD));
            table.add(new TableString("Физкультура", 196, Mark.SET_OFF_FAIL));
            table.add(new TableString("С++", 64, Mark.SET_OFF_PASS));

            Reference reference = new Reference("Москвитина Елена Валерьевна", "ОмГУ",
                    "ИМИТ", "Прикладная математика и информатика", "01.09.2018", "22.06.2022", table);

            assertEquals("Москвитина Елена Валерьевна", reference.getStudent());
            assertEquals("ОмГУ", reference.getUniversity());
            assertEquals("ИМИТ", reference.getFaculty());
            assertEquals("Прикладная математика и информатика", reference.getSpeciality());
            assertEquals("01.09.2018", reference.getBeginDate());
            assertEquals("22.06.2022", reference.getEndDate());
            assertEquals(table, reference.getTable());
        }
}
